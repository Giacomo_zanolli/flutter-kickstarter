import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'core/localization/localizations_manager.dart';
import 'core/navigation/navigator.dart';
import 'core/themes/bloc/theme_bloc.dart';
import 'features/feautre1/presentation/pages/homePage/home_page.dart';

void main() {
  Routes.createRoutes();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ThemeBloc(),
      child: BlocBuilder<ThemeBloc, ThemeState>(
        builder: _buildWithTheme,
      ),
    );
  }

  Widget _buildWithTheme(BuildContext context, ThemeState state) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: state.themeData,
      home: MyHomePage(HomePageArgs("Flutter Demo Home Page")),
      supportedLocales: supportedLocales,
      localizationsDelegates: delegates,
      localeResolutionCallback: chooseLocaleForThisApp,
      onGenerateRoute: Routes.sailor.generator(),
      navigatorKey: Routes.sailor.navigatorKey,
    );
  }
}
