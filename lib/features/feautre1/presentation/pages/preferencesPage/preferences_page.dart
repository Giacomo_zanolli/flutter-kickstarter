import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/localization/localized.dart';
import '../../../../../core/themes/app_themes.dart';
import '../../../../../core/themes/bloc/theme_bloc.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Localized.translate(context, "Preferences"),
        ),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemBuilder: (context, index) {
          final AppTheme itemAppTheme = AppTheme.values[index];
          return Card(
            color: appThemeData[itemAppTheme].primaryColor,
            child: ListTile(
              title: Text(
                itemAppTheme.toString(),
                style: appThemeData[itemAppTheme].textTheme.body1,
              ),
              onTap: () {
                BlocProvider.of<ThemeBloc>(context).add(
                  Themechanged(
                    theme: itemAppTheme,
                  ),
                );
              },
            ),
          );
        },
        itemCount: AppTheme.values.length,
      ),
    );
  }
}
