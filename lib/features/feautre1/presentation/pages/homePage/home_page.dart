import 'package:flutter/material.dart';
import 'package:sailor/sailor.dart';

import '../../../../../core/localization/localized.dart';
import '../../../../../core/navigation/navigator.dart';

class HomePageArgs extends BaseArguments {
  HomePageArgs(this.title);

  final String title;
}

class MyHomePage extends StatefulWidget {
  const MyHomePage(this.args, {Key key}) : super(key: key);

  final HomePageArgs args;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.args.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Routes.sailor.navigate(
                settingsPagePath,
                transitions: [
                  SailorTransition.slide_from_left,
                ],
              );
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              Localized.translate(
                  context, "You have pushed the button this many times:"),
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: Localized.translate(context, "Increment"),
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
