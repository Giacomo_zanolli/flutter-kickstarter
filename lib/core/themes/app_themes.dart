import 'package:flutter/material.dart';

///This file holds the configuration for the themes that will be used throughout
/// the app

enum AppTheme {
  greenLight,
  greenDark,
  blueLight,
  blueDark,
}

final Map<AppTheme, ThemeData> appThemeData = {
  AppTheme.greenLight: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.green,
  ),
  AppTheme.greenDark: ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.green[700],
  ),
  AppTheme.blueLight: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.blue,
  ),
  AppTheme.blueDark: ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.blue[700],
  ),
};
