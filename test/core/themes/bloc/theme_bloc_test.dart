import 'package:flutter_test/flutter_test.dart';
import 'package:myapp/core/themes/app_themes.dart';
import 'package:myapp/core/themes/bloc/theme_bloc.dart';

void main() {
  test("theme changes correctly", () async {
    //Arrange
    const AppTheme appTheme = AppTheme.blueDark;
    final ThemeBloc themeBloc = ThemeBloc();
    //Act
    themeBloc.add(
      const Themechanged(theme: appTheme),
    );
    //Assert
    final ThemeState expectedThemeState =
        ThemeState(themeData: appThemeData[appTheme]);
    expect(themeBloc, emitsThrough(expectedThemeState));
    themeBloc.close();
  });
}
