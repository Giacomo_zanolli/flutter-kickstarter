import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:myapp/core/localization/localizations_manager.dart';

void main() {
  for (Locale requested in supportedLocales) {
    group("handles correctly ${requested.toLanguageTag()}", () {
      test(
        "loads the correct language on full match",
        () async {
          //Act
          final Locale l = chooseLocaleForThisApp(requested, supportedLocales);
          //Assert
          expect(l, requested);
        },
      );
      test("loads the correct language on partial match", () async {
        //Arrange
        requested = Locale(requested.languageCode, "random");
        //Act
        final Locale l = chooseLocaleForThisApp(requested, supportedLocales);
        //Assert
        expect(l.languageCode, requested.languageCode);
      });
    });
  }
  test("loads default language on no match", () async {
    //Arrange
    const Locale requested = Locale("random", "random");
    //Act
    final Locale l = chooseLocaleForThisApp(requested, supportedLocales);
    //Assert
    expect(l, const Locale("en", "GB"));
  });
}
